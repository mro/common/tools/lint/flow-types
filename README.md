# Flow types

Flow Types for APC modules

For documentation about flow, please refer to <https://flow.org/en/docs/types>

## Using type libraries

```bash
# Create a flow-typed directory
mkdir flow-typed

# Add this module
git submodule add -b release https://gitlab.cern.ch/apc/common/tools/lint/flow-types.git flow-typed/common
```

Then run the regular flow installation, adding *flow-typed* in *\[libs\]* section.

**Note:** It is recommended to use [repo-check](https://gitlab.cern.ch/apc/common/tools/repo-check) to automatically set this up.

## Adding type libraries

1.  Add your type libraries in *local*

2.  Write a test file for your library named *test/test_&lt;libname&gt;.js*

3.  Add your library in *package.json* using *npm install*

4.  Run test script using *test/run.js*

**Note:** Modifications must be done on the **master** branch, **release** being reserved for ci-integration.
