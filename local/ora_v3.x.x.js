declare module "ora" {

  declare type $Ora = {
    start(text?: string): $Ora;
    succeed(text?: string): $Ora;
    fail(text?: string): $Ora;
    warn(text?: string): $Ora;
    info(text?: string): $Ora;
    stop(): $Ora;
    stopAndPersist($Shape<{ symbol: string, text: string, prefixText: string }>): $Ora;
    clear(): $Ora;
    render(): $Ora;
    frame(): $Ora;

    promise<T>(action: Promise<T>, options?: {}): Promise<T>;

    text: string;
    prefixText: string;
    color: string;
    spinner: string;
    indent: number;
    isSpinning: bool;
  }

  declare type $OraOptions = {|
    text: string;
    prefixText: string;
    spinner: string|{};
    color: string;
    hideCursor: bool;
    indent: number;
    interval: number;
    stream: any;
    isEnabled: bool;
  |}

  declare function ora(options?: string|$Shape<$OraOptions>): $Ora;
  declare module.exports: typeof ora;
}
