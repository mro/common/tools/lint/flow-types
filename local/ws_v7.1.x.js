// flow-typed signature:
// flow-typed version: ws_v7.1.x/flow_v0.100.0

declare module 'ws' {
  declare function ws$VerifyClient(
    info: { origin: string, req: http$IncomingMessage<>, secure: boolean },
    cb?: ((result: boolean, code: number, name: string, headers: { [string]: any }) => any)): any;

  declare function ws$HandleProtocols(protocols: Array<string>, request: http$IncomingMessage<>): boolean

  declare type ws$PreMessageDeflate = $Shape<{
    serverNoContextTakeover: boolean,
    clientNoContextTakeover: boolean,
    serverMaxWindowBits: number,
    clientMaxWindowBits: number,
    zlibDeflateOptions: any,
    zlibInflateOptions: any,
    threshold: number,
    concurrencyLimit: number
  }>

  declare class WebSocket extends events$EventEmitter {
    constructor(address: string, protocols?: Array<string>, options?: $Shape<{
      followRedirects: boolean,
      handshakeTimeout: number,
      maxRedirects: number,
      preMessageDeflate: boolean|ws$PreMessageDeflate,
      protocolVersion: number,
      origin: string,
      maxPayload: number
    }>): WebSocket;


    binaryType: "nodebuffer"|"arraybuffer"|"fragments";
    bufferAmount: number;
    extensions: {};
    onclose: () => any;
    onerror: () => any;
    onerror: () => any;
    onmessage: () => any;
    onopen: () => any;
    protocol: string;

    on(type: string, listener: () => any): this;
    once(type: string, listener: () => any): this;
    addEventListener(type: string, listener: () => any): void;
    removeEventListener(type: string, listener: () => any): void;
    removeAllListeners(event?: string): this;

    close(code: number, reason: string): void;
    ping(data: any, mask?: boolean, callback?: () => any): void;
    pong(data: any, mask?: boolean, callback?: () => any): void;
    send(data: any,
         options?: $Shape<{ compress: boolean, binary: boolean, mask: boolean, fin: boolean }>,
         callback?: () => any): void;
    terminate(): any;

    static createWebSocketStream(websocket: WebSocket, options?: $Shape<{
      allowHalfOpen: boolean,
      readableObjectMode: boolean,
      writableObjectMode: boolean,
      readableHighWaterMark: number,
      writableHighWaterMark: number
    }>): stream$Duplex;

    static Server(options?: $Shape<{
      host: string,
      port: number,
      backlog: number,
      server: http$Server|https$Server,
      verifyClient: typeof ws$VerifyClient,
      handleProtocols: typeof ws$HandleProtocols,
      path: string,
      noServer: boolean,
      clientTracking: boolean,
      perMessageDeflate: boolean|ws$PreMessageDeflate,
      maxPayload: number
    }>, callback: () => any): WebSocketServer;
  }

  declare class WebSocketServer extends events$EventEmitter {

    clients?: Set<any>; // only when clientTracking is enabled

    address(): { port: number, family: string, address: string };
    close(callback?: () => any): any;
    handleUpgrade(request: http$IncomingMessage<>, socket: net$Socket, head: Buffer, callback: (ws: WebSocket) => any): void;
    shouldHandle(request: http$IncomingMessage<>): boolean;
  }

  declare module.exports: typeof WebSocket;
}
