// flow-typed signature:
// flow-typed version: progress_v2.x.x/flow_v0.100.0

declare module 'progress' {

declare type Progress$Options = {
  width?: number,
  head?: string,
  complete?: string,
  incomplete?: string,
  renderThrottle?: number,
  clear?: boolean,
  total?: number,
  stream?: any,
  curr?: number,
  callback?: function,
  ...
}

declare class ProgressBar {
  constructor(format: string, opts: Progress$Options): ProgressBar;
  tick(progres: number, tokens: ?{...}): void;
}

declare module.exports: typeof ProgressBar

}
