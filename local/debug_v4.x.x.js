// flow-typed signature:
// flow-typed version: debug_v4.x.x/flow_v0.100.0

declare module 'debug' {
  declare module.exports: {
    (name: string): (format: string, ...args: Array<mixed>) => void,
    formatters: { [string]: (arg: any) => string },
    useColors: () => false
  }
}
