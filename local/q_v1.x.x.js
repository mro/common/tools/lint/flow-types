// flow-typed signature:
// flow-typed version: q_v1.x.x/flow_v0.100.0

declare class QPromise<+R> extends Promise<R> {
  then<U>(onFulfilled: ?(value: R) => Promise<U>|QPromise<U>|U, onRejected?: ?(error: any) => any): QPromise<U>;
  catch<U>(onRejected: ?(error: any) => QPromise<U>|U): QPromise<U>;
  finally(callback: () => any) : QPromise<R>;
  done(onFulfilled: ?(value: R) => any, onRejected?: ?(error: any) => any): QPromise<R>;

  delay(ms: number): QPromise<R>;
  nodeify(callback: ?(err: any, value: any) => any) : QPromise<R>;

  thenResolve<U>(value: U): QPromise<U>;
  thenReject(value: any): QPromise<R>;
  tap(onFulfilled: ?(value: R) => any): QPromise<R>;

  timeout(ms: number, message: ?string): QPromise<R>;

  isPending(): boolean;
  isFulfilled(): boolean;
}
declare type q$Promise<T> = QPromise<T>

type ExtractPromise = <V>(Promise<V>) => V
declare type Q = {
  <T>(arg?: T): QPromise<T>,

  delay<T>(ms: number): QPromise<T>,
  defer<T>(): QDeffered<T>,

  reject<T>(value: any): QPromise<T>,

  nfbind<T>(nodeFunc: (...args: any) => T, ...args: any): QPromise<T>,
  nfapply<T>(nodeFunc: (...args: any) => T, args: Array<any>): QPromise<T>,

  nbind<T>(method: (...args: any) => T, thisArg: any, ...args: any): QPromise<T>,
  ninvoke<T>(object: any, methodName: string, ...args: any): QPromise<T>,

  nfcall<T>(fun: () => any, ...args: any): QPromise<T>,

  all<A>(promises: A): QPromise<$TupleMap<A, ExtractPromise>>
}

declare class QDeffered<+R> {
  resolve(value: Promise<R>|QPromise<R>|R): void;
  reject(value: any): void;
  promise: QPromise<R>;
}
declare type q$Deffered<T> = QDeffered<T>

declare module 'q' {
  declare export type QPromise<T> = QPromise<T>
  declare export type QDeffered<T> = QDeffered<T>
  declare module.exports: Q;
}
