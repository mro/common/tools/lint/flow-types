// flow-typed signature:
// flow-typed version: vue_v2.x.x/flow_v0.100.0

type object = { [key: any]: any };

declare module 'vue' {

  declare type Vue$VNode = any //FIXME

  declare type Vue$Prop = {
    type: typeof String | typeof Number | typeof Boolean | typeof Array | typeof Object | typeof Date | typeof Function | typeof Symbol,
    default?: any,
    required?: boolean,
    validator?: (value: any) => boolean
  }

  declare type Vue$Watch = string |
    (val: any, old: any) => void |
    { handler: (val: any, old: any) => void, deep?: boolean, immediate?: boolean }

  declare type Vue$Component = $Shape<{
    name: string,
    delimiters: Array<string>,
    functional: boolean,
    model: { prop?: string, event?: string },
    inheritAttrs: boolean,
    comments: boolean,

    mixins: Array<Vue$Mixin>,

    beforeCreate: () => void,
    created: () => void,
    beforeMount: () => void,
    mounted: () => void,
    beforeUpdate: () => void,
    updated: () => void,
    activated: () => void,
    deactivated: () => void,
    beforeDestroy: () => void,
    destroyed: () => void,
    errorCaptured: () => void,

    directives: { [key : string]: () => void },
    filters: { [key : string]: () => void },
    components: { [key: string]: Vue$Component },
    parent: Vue$Component,
    extends: Vue$Component,

    data: (() => object) | object,
    props: Array<string> | { [key: string]: Vue$Prop },
    propsData: { [key: string]: any },
    computed: {
      [key: string]: (() => any | { get: () => any, set: (value: any) => void })
    },

    methods: { [key: string]: () => any },
    watch: { [key: string]: Vue$Watch | Array<Vue$Watch> },
    el: string | any,
    template: string,
    render: (createElement: () => any) => any,
    renderError: (createElement: () => any, error: Error) => any,
  }>

  declare class Vue$Instance {
    constructor(component: Vue$Component): Vue$Instance;

    static extend(options: {}): Vue$Instance;
    static nextTick(callback: ?() => any, context: ?object): Promise<object|void>;
    static set(target: object|Array<any>, property: string|number, value: any): void;
    static delete(target: object|Array<any>, property: string|number): void;
    static directive(id: string, definition: ?(function|object)): void;
    static filter(id: string, definition: ?function): void;
    static component(id: string, definition: Vue$Component): void;
    static use(plugin: Vue$Plugin, options: ?object): void;
    static mixin(mixin: Vue$Mixin): void;
    static compile(template: string): { render: () => void }; //FIXME
    static observable(object: object): object;
    static version: string;


    $data: object;
    $props: object;
    $el: any;
    $options: object;
    $parent: Vue$Instance;
    $root: Vue$Instance;
    $children: Array<Vue$Instance>;
    $slots: { [key: string]: ?Array<Vue$VNode> };
    $scopedSlots: { [key: string]: () => ?Array<Vue$VNode> };
    $refs:  { [key: string]: ?Array<Vue$Instance> };
    $isServer: boolean;
    $attrs: { [key: string]: string };
    $listeners: { [key: string]: () => void | Array<() => void> };

    $watch(expOrFn: string | () => void, callback: () => any, options: ?{ deep?: boolean, immediate?: boolean}): (() => void);
    $set(target: object|Array<any>, property: string|number, value: any): void;
    $delete(target: object|Array<any>, property: string|number): void;

    $on(event: string | Array<string>, callback: () => any): void;
    $once(event: string | Array<string>, callback: () => any): void;
    $off(event: string | Array<string>, callback: ?() => any): void;
    $emit(event: string, ...args: any): void;

    $mount(elementOrSelector: string | any, hydrating: ?boolean): void;
    $forceUpdate(): void;
    $nextTick(callback: ?() => any): void;
    $destroy(): void;
  }

  declare type Vue$Mixin = Vue$Component;

  declare type Vue$Plugin = {
    install: (Vue: typeof Vue$Instance, options: ?object) => void;
  }

  declare export default typeof Vue$Instance;
}
