

// TODO: use core repo's Component type
export type Vue$Component = {
  [key: string]: any,
  $refs: { [key: string]: Vue$Component & Array<Vue$Component> }
};

/**
 * Utility type for a selector
 */
export type Vue$Selector =
    string
  | Vue$Component
  | {| name: string |}
  | {| ref: string |};

export type Vue$ComponentSelector =
  Vue$Component
  | {| name: string |}
  | {| ref: string |};

/**
 * Utility type for slots
 */
export type Vue$Slots = {
  [key: string]: Array<Vue$Component | string> | Vue$Component | string
}

/**
 * Utility type for stubs which can be a string of template as a shorthand
 * If it is an array of string, the specified children are replaced by blank components
 */
export type Vue$Stubs = {
  [key: string]: Vue$Component | string | boolean
} | string[]

/**
 * Base class of Wrapper and WrapperArray
 * It has common methods on both Wrapper and WrapperArray
 */
declare class BaseWrapper {
  contains(selector: Vue$Selector): boolean;
  exists(): boolean;
  isVisible(): boolean;

  attributes(): { [name: string]: string };
  attributes(key: string): string | void;
  classes(): Array<string>;
  classes(className: string): boolean;
  props(): { [name: string]: any };
  props(key: string): any | void;

  is(selector: Vue$Selector): boolean;
  isEmpty(): boolean;
  isVueInstance(): boolean;

  setData(data: {}): void;
  setMethods(data: {}): void;
  setProps(props: {}): void;

  setValue(value: any): void;
  setChecked(checked?: boolean): void;
  setSelected(): void;

  trigger(eventName: string, options?: {}): void;
  destroy(): void;
}

declare class VueWrapper<V> extends BaseWrapper {
  vm: V;
  element: HTMLElement;
  options: WrapperOptions;

  find(selector: Vue$Selector): VueWrapper<any>;
  findAll(selector: Vue$Selector): VueWrapperArray<any>;
  findComponent(selector: Vue$ComponentSelector): VueWrapper<any>;
  findAllComponents(selector: Vue$ComponentSelector): VueWrapperArray<any>;

  html(): string;
  text(): string;
  name(): string;

  emitted(): { [name: string]: Array<Array<any>> };
  emitted(event: string): Array<any>;
  emittedByOrder(): Array<{ name: string, args: Array<any> }>;
}
declare type Vue$Wrapper = VueWrapper<Vue$Component>;

declare class VueWrapperArray<V> extends BaseWrapper {
  length: number;
  wrappers: Array<VueWrapper<V>>;

  at(index: number): VueWrapper<V>;
  filter(
    predicate: (
      value: VueWrapper<V>,
      index: number,
      array: VueWrapper<V>[]
    ) => any
  ): VueWrapperArray<any>;
}
declare type Vue$WrapperArray = VueWrapperArray<Vue$Component>;

type WrapperOptions = {
  attachedToDocument?: boolean;
  sync?: boolean;
}

type MountOptions = {
  attachToDocument?: boolean;
  localVue?: any;
  mocks?: {} | false;
  parentComponent?: Vue$Component;
  slots?: Vue$Slots;
  stubs?: Vue$Stubs | false;
  sync?: boolean;
}

declare module "@vue/test-utils" {
  declare type Wrapper = Vue$Wrapper;
  declare type Component = Vue$Component;
  declare function createLocalVue(): any;
  declare function mount(component: {}, options?: MountOptions): Vue$Wrapper;
  declare function createWrapper(node: Component, options?: WrapperOptions): Vue$Wrapper;
}
