#!/usr/bin/env sh

die() {
    echo $1 >&2
    exit 1
}

for f in test/test_*; do
    (cat "$f" | flow check-contents) || die "Failed to validate ${f}"
done
