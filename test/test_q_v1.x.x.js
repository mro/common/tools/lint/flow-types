// @flow

import { describe, it } from "flow-typed-test";

import q from 'q'

describe('q', () => {
  it('can create a promise chain', () => {
    var prom = q(42)
    .then((value: number) => '42')
    .then((value: string) => Promise.resolve(12))
    .tap((value: number) => null)
    .delay(10)
    .thenResolve('test');

    (prom: QPromise<string>)
  });

  it('can catch errors', function() {
    var prom = q(42)
    .catch((err /*: { message: string } */) => err.message)
    .then((value: string) => value, (err /*: number */) => null);

    (prom: QPromise<string>)
  });

  it('can create a deffered', () => {
    var def: q$Deffered<number> = q.defer();

    def.resolve(12);
    def.reject('test');
    def.reject();

    /* $FlowExpectedError */
    def.resolve('42');
    (def.promise: q$Promise<number>)
  });

  it('can declare a function returning regular promise', () => {
    function foo(): Promise<number> {
      return q(42);
    }
  });

  it('can resolve an array of promnises', () => {
    var ret = q.all([ q(12), q().thenResolve('test') ])
    .then((ret) => {
      (ret[0]: number);
      (ret[1]: string);
      /* $FlowExpectedError */
      (ret[1]: number);
    });
  });
});
