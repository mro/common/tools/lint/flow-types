// @flow

import { describe, it } from "flow-typed-test";

import Vue from 'vue';

describe('vue', () => {
  it('can use static methods', () => {
    Vue.nextTick((context) => null, { test: 42 });
    Vue.nextTick().then(() => null);

    Vue.use({
      install: (vue: typeof Vue) => vue.filter("inc", (value) => value + 1)
    });
  });

  it('can create an instance', () => {
    // from https://vuejs.org/v2/guide/index.html
    var app2 = new Vue({
      el: '#app-2',
      data: {
        message: 'You loaded this page on ' + new Date().toLocaleString()
      }
    });
    var message: string = app2.$data.message;

    // from: https://vuejs.org/v2/guide/components.html
    Vue.component('button-counter', {
      data: function(): { [key: string]: any } {
        return { count: 0 }
      },
      template: '<button v-on:click="count++">You clicked me {{ count }} times.</button>'
    })
  });
});
