// @flow

import { describe, it } from "flow-typed-test";
const Vuex = require('vuex');

describe('vuex', () => {
  // from https://vuex.vuejs.org/guide/modules.html
  const moduleA : Vuex.Module<{count: number}, any> = {
    state: () => ({ count: 0 }),
    mutations: {
      increment (state) { state.count++ }
    },
    getters: {
      doubleCount (state) { return state.count * 2 }
    }
  };

  const storeOptions : Vuex.StoreOptions<{sample: number}> = {
    namespaced: true,
    state: { sample: 42 },
    modules: { moduleA }
  };

  it('can declare a store', () => {
    let store = new Vuex.Store({});
    store = new Vuex.Store<{sample: number}>(storeOptions);

    // untyped store
    store = new Vuex.Store(storeOptions);
    store.state.sample = 42;
    /* $FlowExpectedError */
    store.state.nope = 42;
  });

  it('can map states', () => {
    Vuex.mapState({
      count: state => state.count,
      countAlias: 'count',
      countPlusLocalState(state) {
        return state.count + this.localCount;
      }
    });
    Vuex.mapState('module', { count: state => state.count });

    Vuex.mapState([ 'count' ]);
    Vuex.mapState('module', [ 'count' ]);
  });
});
