// @flow

import { describe, it } from "flow-typed-test";

import { mount, createWrapper } from '@vue/test-utils';

describe('@vue/test-utils', () => {
  it('can mount a component', () => {
    var wrapper = mount({ template: '<div></div>' });
    (wrapper: Vue$Wrapper);
    wrapper.exists() === true;

    wrapper.attributes().id;
    wrapper.attributes('id');

    wrapper.classes()[0] === 'bar';
    wrapper.classes('bar') === true;

    wrapper.contains('p') === true;

    (wrapper.emitted().foo: Array<any>)

    wrapper.emittedByOrder()[0].name === 'foo';

    wrapper.find('p').is('p') === true;
    wrapper.findComponent({ name: 'p' }).is('p') === true;
    wrapper.find({ name: 'bar' }).name() === 'bar';
    wrapper.find({ ref: 'bar' });

    wrapper.findAll('p').at(0).exists() === true;
    wrapper.findAll('p').length === 1;
    wrapper.findAllComponents({ ref: 'test' }).length === 1;

    wrapper.html() === '<div></div>';
    wrapper.isEmpty() === false;
    wrapper.isVisible() === false;
    wrapper.find('a').isVueInstance() === false;

    (wrapper.props('test'): any)
    wrapper.props()['test'] === 42;

    wrapper.setChecked();
    wrapper.setChecked(true);

    wrapper.trigger('test');
    wrapper.setData({ foo: 'bar' });

    wrapper.vm.test;
    (wrapper.vm.$refs.test: Vue$Component)
    var comp: Vue$Component = wrapper.vm.$refs.testArray[0];

    wrapper.destroy();
  });

});
