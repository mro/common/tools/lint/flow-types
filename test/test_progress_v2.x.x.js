// @flow

import { describe, it } from "flow-typed-test";
const ProgressBar = require('progress');

describe('progress', () => {
  it('can create a progres bar', () => {
    var progress = new ProgressBar('Downloading [:bar] :percent (ETA::etas) ', {
      width: 40, complete: '=', inncomplete: '-', renderThrottle: 250,
      head: '>', clear: true,
      total: 100, stream: process.stdout,
      custom: 12
    });
    progress.tick(12);
    progress.tick(14, { custom: 12 });
    
    /* $FlowExpectedError */
    progress = new ProgressBar('', { width: '40' });
  });
});