//@flow
const ora = require('ora');

var spinner = ora('Loading unicorns').start();

spinner.text = 'loaded';
spinner.prefixText = 'test';
spinner.color = 'blue';
spinner.indent = 2;

if (spinner.isSpinning === true) {
  spinner.stop();
}

spinner.start('restarting...');
spinner.succeed('success !');
spinner.fail();
spinner.warn('Warning !');
spinner.info('Info: blah');
spinner.stopAndPersist({ text: 'blah' });

spinner.clear().render().frame();

spinner.promise(new Promise(function(resolve) {
  resolve(42);
}))
.then(function() { spinner.start(); })
