// @flow

import { describe, it } from "flow-typed-test";
const WebSocket = require('ws');

function fakeCallback() {}

describe('ws', () => {
  it('can use WebSocket object', () => {
    let ws = new WebSocket("ws://javascript.info");
    ws.on("close", fakeCallback);
    ws.once("message", fakeCallback);
    ws.addEventListener("error", fakeCallback);
    ws.addEventListener("event", fakeCallback);
    ws.removeAllListeners("error");
    ws.removeEventListener("event", fakeCallback);
  });
});
